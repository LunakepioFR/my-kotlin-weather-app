package com.example.myapplication

import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {
    lateinit var fusedLocationProviderClient : FusedLocationProviderClient
    lateinit var currentLocation : Location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val description = findViewById<View>(R.id.description) as TextView
        val windSpeed = findViewById<View>(R.id.windSpeed) as TextView
        val humidity = findViewById<View>(R.id.humidity) as TextView
        val currTemp = findViewById<View>(R.id.temp) as TextView
        Log.d("MainActivity", "Coucou je suis dans le OnCreate()")
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)


        findViewById<Button>(R.id.btn_get_location).setOnClickListener{
            fetchLocation()
        }



        try {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val weatherService: WeatherService = retrofit.create(WeatherService::class.java)
            val call = weatherService.getWeatherByName(name = "Paris")

            call.enqueue(object : Callback<Weather> {
                override fun onResponse(call: Call<Weather>, response: Response<Weather>) {
                    description.text = response.body()?.weather?.first()?.description
                    humidity.text = "Humidité :" + response.body()?.main?.humidity.toString() + "%"
                    currTemp.text = response.body()?.main?.temp.toString() + "°C"
                    windSpeed.text = "Vit. du vent : " + response.body()?.wind?.speed.toString() + " Km/h"
                    Log.i("MainActivityTest", "Test api" + response.body().toString())
                    Log.i("Test", call.request().url().toString())
                }

                override fun onFailure(call: Call<Weather>, throwable: Throwable) {
                    Log.e("MainActivityError", throwable.message.toString())
                }


            })

        } catch (e: Exception) {
            Log.d("MainActivity", e.message.toString())
        }
    }

    private fun fetchLocation() {
        val task = fusedLocationProviderClient.lastLocation
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat
                .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 101)
            return
        }
        task.addOnSuccessListener {
            if (it != null) {
                val locationText = findViewById<View>(R.id.location) as TextView
                locationText.text = "${it}"
                Log.d("location", "${it}")

            }
        }


    }

    override fun onStart() {
        super.onStart()
        Log.d("MainActivity", "je suis dans le start")
    }

    override fun onPause() {
        super.onPause()
        Log.d("MainActivity", "je suis dans la pause")
    }
}