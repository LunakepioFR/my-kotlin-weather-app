package com.example.myapplication

data class Weather (
    var weather: ArrayList<WeatherData>,
    var main: Main,
    var wind: Wind,
)

data class WeatherData(
    val description: String,
)

data class Main(
    val temp: Float,
    val humidity: Int,
)

data class Wind(
    val speed: Float,
)